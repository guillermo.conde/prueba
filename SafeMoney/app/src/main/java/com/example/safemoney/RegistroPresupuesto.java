package com.example.safemoney;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;

import static java.util.Calendar.*;

public class RegistroPresupuesto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_presupuesto);


        TextView vigencia = (TextView) findViewById(R.id.vigPresup);//Enlazamos el TextView del archivo xml.
         vigencia.setOnClickListener(new View.OnClickListener() { //Le asignamos un evento onClick, para que haga algo al hacerle click.
             @Override
             public void onClick(View v) {
                 Intent despliege = new Intent(RegistroPresupuesto.this, RegistroPresupuesto.class);//Le asignamos una intencion (sin esto no funciona el click).
                 Calendar c = Calendar.getInstance();//Declaramos un objeto de tipo calendario.
                 //Obtenemos dia, mes y año
                 int dia = c.get(Calendar.DAY_OF_MONTH);
                 int mes = c.get(Calendar.MONTH);
                 int anio = c.get(Calendar.YEAR);
                 //Inicializamos un objeto de calendario emergente que permite tomar la fecha seleccionada por el usuario.
                 DatePickerDialog calendario = new DatePickerDialog(RegistroPresupuesto.this, new DatePickerDialog.OnDateSetListener(){
                     @Override
                     public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) { //Metodo para extraer la fecha que indica el usuario.
                          String fSeleccionada = dayOfMonth +"/"+ (month+1) +"/"+ year; //Concatenamos la fecha en formato numérico
                          vigencia.setText(fSeleccionada);//Colocamos la fecha en el campo de texto.
                     }
                 }, anio, mes, dia);//Indicmos la fecha en que inicia el calendario.
                 calendario.show(); //Disparamos el calendario.
             }
         });
    }



    public void onClick(View view){
        Intent accion = null; //Declaramos una accion que esta vacia por el momento.
        switch (view.getId()){ //Validando el id que se reciba para saber como proceder.
            case R.id.btnCancPresup:
                finish();//Culminacion de la activity
                break;
            case R.id.btnRegPresup:
                if (valCampoVacio() == true){
                    finish();
                }
                break;
        }

    }

    /*Validacion de que se ingresen todos los datos
    * return boolean que define si se continua con el proceso o no*/
    public boolean valCampoVacio(){
        boolean bandera = true;
        //Obtenemos los campos
        EditText entrada1 = findViewById(R.id.txtMtoPresup);
        TextView entrada2 = findViewById(R.id.vigPresup);
        //Obtenemos el valor de los campos
        String cadena1 = entrada1.getText().toString();
        String cadena2 = entrada2.getText().toString();

        if (cadena1.isEmpty()){//Validacion de si el campo esta vacio
            entrada1.setError("El monto no puede quedar vacío");//Mensaje de error.
            bandera = false;
        }
        if (cadena2.isEmpty()){//Validacion de si el campo esta vacio
            entrada2.setError("La fecha de vigencia no puede quedar vacía");//Mensaje de error.
            bandera = false;
        }
        return bandera;
    }
}