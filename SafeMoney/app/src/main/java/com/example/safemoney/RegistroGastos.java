package com.example.safemoney;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class RegistroGastos extends AppCompatActivity {
    Spinner categorias;
    int dia, mes , anio, hora, minuto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_gastos);
        categorias = (Spinner) findViewById(R.id.categorias);
        ArrayAdapter<CharSequence> adaptador = ArrayAdapter.createFromResource(this, R.array.comboCategorias, android.R.layout.simple_spinner_item);
        categorias.setAdapter(adaptador);

    }

    public void onClick(View view){
        Intent accion = null;
        switch (view.getId()){
            case R.id.btnCancGto:
                finish();
                break;
            case R.id.btnRegistrarGto:
                if (valCampoVacio() == true){
                    obtenerTmpo();
                    Toast.makeText(RegistroGastos.this, dia+"/"+mes+"/"+anio+" Y son las: "+hora+":"+minuto,Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    /*Validacion de que se ingresen todos los datos
     * return boolean que define si se continua con el proceso o no*/
    public boolean valCampoVacio(){
        boolean bandera = true;
        //Obtenemos los campos
        EditText entrada1 = findViewById(R.id.txtMtoGto);
        TextView entrada2 = findViewById(R.id.txtConceptoGto);
        //Obtenemos el valor de los campos
        String cadena1 = entrada1.getText().toString();
        String cadena2 = entrada2.getText().toString();

        if (cadena1.isEmpty()){//Validacion de si el campo esta vacio
            entrada1.setError("El monto no puede quedar vacío.");//Mensaje de error.
            bandera = false;
        }
        if (cadena2.isEmpty()){//Validacion de si el campo esta vacio
            entrada2.setError("El concepto no puede quedar vacío.");//Mensaje de error.
            bandera = false;
        }
        return bandera;
    }

    public void obtenerTmpo(){
        Calendar calendario = Calendar.getInstance();
        dia = calendario.get(Calendar.DAY_OF_MONTH);
        mes = calendario.get(Calendar.MONTH)+1;
        anio = calendario.get(Calendar.YEAR);
        hora = calendario.get(Calendar.HOUR_OF_DAY);
        minuto = calendario.get(Calendar.MINUTE);

    }
}