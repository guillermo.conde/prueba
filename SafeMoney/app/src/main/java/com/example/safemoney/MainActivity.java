package com.example.safemoney;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view){
        Intent accion = null;
        switch (view.getId()){
            case R.id.btnGtos:
                accion = new Intent(MainActivity.this, RegistroGastos.class);
                break;
            case R.id.btnPresup:
                accion = new Intent(MainActivity.this, RegistroPresupuesto.class);
                break;
        }
        startActivity(accion);
    }
}